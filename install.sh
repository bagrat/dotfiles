#!/bin/bash

# Find the absolute path to dotfiles
DOTFILES="`dirname \`realpath install.sh\``"

function maybe_backup {
    DEST="$1"

    # Check if the destination file exists, and backup in that case
    if [ -e "$DEST" ]
    then
        echo "'$DEST' already exists, backing up..."
        mv "$DEST" "$DEST.bak.`date +%s`"
    fi
}

function install_file {
    SRC="$1"
    DEST="$2"

    echo "installing file: $DEST"

    maybe_backup "$DEST"
    ln -s "$SRC" "$DEST"
}

if [ "$IS_MAC" == "1" ] && [ "$SKIP_BREW" != "1" ]
then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew bundle
fi

FILES=(
".bashrc"
".inputrc"
"Brewfile"
".tmux.conf"
".vim"
)

for FILE in ${FILES[@]}
do
    install_file "$DOTFILES/$FILE" "$HOME/$FILE"
done

if [ "$IS_MAC" == "1" ]
then
    ln -sf ~/.bashrc ~/.profile
fi

vim +PlugInstall +qal

export EXTRA_PATH=~/.bashrc-extra

echo "" >> $EXTRA_PATH
echo "export DOTFILES=$DOTFILES" >> $EXTRA_PATH

export PRIVATE_PATH=${PRIVATE_PATH:-"$HOME/drive/etc/private"}
if [ ! -z $PRIVATE_PATH ]
then
    echo "" >> $EXTRA_PATH
    echo "export PRIVATE_PATH=$PRIVATE_PATH" >> $EXTRA_PATH
fi

export GITCONFIG_TEXT=$(cat $DOTFILES/.gitconfig)

# Have to do this to expand the env vars in the gitconfig
bash -c "echo \"$GITCONFIG_TEXT\" > ~/.gitconfig"
