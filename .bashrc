#!/bin/bash

# When on my personal laptop, I do not need the hostname in the prompt
HOSTNAME_REGEX="^(bagrat13|Bagrat-Aznauryan-.*).*$"
if [[ "`hostname`" =~ $HOSTNAME_REGEX ]]
then
    export HELLO_ITS_ME=1
else
    export HELLO_ITS_ME=0
fi

# Provide a way to turn off patched font symbols
if [[ "$USE_SYMBOLS" != "0" ]]
then
    export USE_SYMBOLS=1
fi

# Find out if we are on a Mac or Linux
MY_KERNEL=`uname -s`
case "$MY_KERNEL" in
    Linux)
        export IS_LINUX=1
        export IS_MAC=0
        ;;
    Darwin)
        export IS_LINUX=0
        export IS_MAC=1
        ;;
    *)
        export IS_LINUX=0
        export IS_MAC=0
        ;;
esac

export EXTRA_PATH=~/.bashrc-extra
source $EXTRA_PATH

# Source the rest of the config files
for file in $DOTFILES/bashrc.d/*
do
    if [ -f "$file" ]
    then
        source $file
    fi
done

# Source other custom files
for file in $HOME/.bashrc.d/*
do
    if [ -f "$file" ]
    then
        source $file
    fi
done

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
