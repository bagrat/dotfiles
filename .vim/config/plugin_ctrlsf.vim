vmap <C-F> <Plug>CtrlSFVwordExec
nmap <C-F> <Plug>CtrlSFPrompt
imap <C-F> <Esc><Plug>CtrlSFPrompt

let g:ctrlsf_position = 'right'
