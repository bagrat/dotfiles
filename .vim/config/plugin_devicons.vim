let g:WebDevIconsOS = 'Darwin' 

let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1

let g:WebDevIconsUnicodeDecorateFolderNodesDefaultSymbol = "\uf07b"
let g:DevIconsDefaultFolderOpenSymbol = "\uf114"

let g:webdevicons_conceal_nerdtree_brackets = 1
let g:WebDevIconsNerdTreeBeforeGlyphPadding = ""
