noremap <leader>t :NERDTreeToggle<CR>

let g:NERDTreeMinimalUI = 1
let g:NERDTreeAutoDeleteBuffer = 1
let g:NERDTreeShowHidden = 1

let g:NERDTreeDirArrowExpandable = "\u00a0"
let g:NERDTreeDirArrowCollapsible = "\u00a0"
" let g:NERDTreeNodeDelimiter = "\u00a0"

let g:NERDTreeIndicatorMapCustom = {
            \ "Modified"  : $SYMGITMODIFIED,
            \ "Staged"    : $SYMGITSTAGED,
            \ "Untracked" : $SYMGITUNTRACKED,
            \ "Renamed"   : $SYMGITRENAMED,
            \ "Unmerged"  : $SYMGITUNMERGED,
            \ "Deleted"   : $SYMGITDELETED,
            \ "Dirty"     : $SYMGITDIRTY,
            \ "Clean"     : "",
            \ "Unknown"   : ""
            \ }

let g:NERDTreeCascadeSingleChildDir = 0
let g:NERDTreeRespectWildIgnore = 1
"let g:NERDTreeStatusline
"
hi! NERDTreeCWD ctermfg=241 ctermbg=8

augroup nerdtreeconceal
    autocmd!
    autocmd FileType nerdtree setlocal conceallevel=3
    autocmd FileType nerdtree setlocal concealcursor=nvic
    autocmd FileType nerdtree syntax match NERDTreeDirSlash #/$# containedin=NERDTreeDir conceal contained
    autocmd FileType nerdtree syntax match NERDTreeExecAsterisk #*$# containedin=NERDTreeExecFile conceal contained
    autocmd FileType nerdtree syntax clear NERDTreeFlags
augroup END
